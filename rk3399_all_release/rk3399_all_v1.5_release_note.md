
V1.5版本更新记录:
1. 修复DP电视兼容性问题。
2. 修复u-boot启动时hdmi电视变色问题。
3. 新增baseparameter分区，用于显示参数的修改和保存。
4. 解决uart4当串口时，u-boot会卡死问题。
5. 解决某些应用场景只用到cpu的大核导致的性能不够的问题。
6. 低温抬压门限修改为0度，原来为10度。
7. linux升级工具以及写号工具更新，解决客户碰到的一些问题。
8. 解决wifi导致的概率性死机问题。
9. 解决切换分辨率时vop导致的概率性内核崩溃问题。
10. 解决typec插拔碰到的一些问题。
11. hwcomposer更新，解决客户碰到的一些问题。
